---
title:  Automation of Quality Control for Global Ocean Data
header:

    overlay_image: /assets/images/2019-12-17-salacia-figure3.jpg
---

# Motivation

This project is part of a large European program, the [SeaDataNet](https://en.wikipedia.org/wiki/SeaDataNet) (2004-2016) and [SeaDataCloud](https://www.ecosia.org/search?q=seadatacloud) (2016-2020) initiatives, which have the aim to provide quality controlled ocean data via web services. Since 2004, more than 100 European data centers have been included, which provide rich data and meta data collections of several variables like temperature, salinity, oxygen, nitrate etc. for the global ocean with focus on the European seas (Figure 1). 

![]({{site.baseurl}}/assets/2019-12-17-salacia-figure1.png)  
__Figure 1__: The research vessel (left) and the equipment (right) to measure variables such as; temperature, salinity, oxygen etc.

In order to provide oceanographic data for research purposes, each sample of the measured variables (temperature, salinity, oxygen, nitrate etc.) is needed to be flagged as ‘good’ or ‘bad’ by quality control (QC) experts manually and/or visually. Figure 2 shows the Ocean Data View Software ([ODV](https://odv.awi.de)) that allows the QC experts to label data with quality flags (QF). Since the size of the dataset is enormous (ca. 9 million profiles) and it is expected to grow significantly in the future, the automation/semi-automation of the QC is a necessity for the ocean science community.

![]({{site.baseurl}}/assets/2019-12-17-salacia-figure2.png)  
__Figure 2__: Manual and Visual quality control of temperature using the Ocean Data View (ODV) software.

Thus, the main motivation of our project is to reduce the amount of work for the scientists. To automate the QC, we use state-of-the-art methods from the field of artificial intelligence, specifically deep learning algorithms.

In a nutshell, our project is a binary classification problem aiming at detecting outliers on measured temperature data in millions of oceanographic samples. For this reason, we design a Multilayer Perceptron ([MLP](https://en.wikipedia.org/wiki/Multilayer_perceptron)) neural network which is a class of feed-forward artificial neural network ([ANN](https://en.wikipedia.org/wiki/Artificial_neural_network)).


# The dataset  

As it is indicated, in our dataset, several variables are provided by European data centers. We are using the outstanding SeaDataNet global dataset of ocean measurements. For the six SeaDataNet regions, Black Sea, Mediterranean, Atlantic, North Sea, Baltic and Arctic, we have Temperature and Salinity profiles. These datasets comprise measurements from the last 100 years and provide a valuable source for ocean science, management, education, government and economy.

In addition to measured variables, we have also spatial and time-wise parameters to define the location and date of the measurements (Figure 3).

![]({{site.baseurl}}/assets/2019-12-17-salacia-figure3.png)  
__Figure 3__: World ocean data that has been supplied by European data centers. (Red dots represent the location of measurements)

Note that, each measurement in Figure 3 has been flagged by the experts using the ODV software (Figure 2) manually and visually, so that, the centers also provide the flags of temperature.

![]({{site.baseurl}}/assets/2019-12-17-salacia-figure4.png)  
__Figure 4__: Temperature vs. depth from the Mediterranean and Black Sea regions (green 'good' quality flags and red 'bad' quality flags)

At the end, the data that we use for training the neural network and testing consist of several features. For instance; latitude, longitude, year, month, depth, temperature, salinity, density, vertical gradient of temperature as a function of depth. All these features and quality flags are included in our input dataset. It is important to note that we deal with highly imbalanced dataset, i.e. the good quality flags are the majority of the whole datasets.


# Training the neural network

## Preprocessing

Firstly, one big step towards designing an architecture for the neural network is the preprocessing of the dataset. We deal with several millions of samples, and we need to design a stable preprocessing frame-work. Similar to the training dataset, new unknown datasets, which are evaluated in an operational environment have to be preprocessed with the same scheme. Our steps for preprocessing can be listed as follows:  

1. Removing profiles, which include only one measurement (sample)
2. Removing samples, which have a missing value for temperature or depth
3. Normalizing and scaling the dataset

## Training the neural network

After preprocessing, we start to train the neural network and check for overfitting to iterate to meaningful parameters and hyperparameters (Figure 5).

![]({{site.baseurl}}/assets/2019-12-17-salacia-figure5.png)  
__Figure 5__: Accuracy and loss plots of our Classification.

## Predictions

Finally, the trained model has to be evaluated using so-called control datasets to assess the prediction quality of the model. The idea is to implement the final model as a public web service for the efficient online QC of ocean data.


# Why we joined to the Hackathon

We hope to get feedbacks regarding to our neural networks' architecture, i.e. how many hidden layers and how many nodes we need. We expect to learn a lot from the experts in the field, especially to avoid typical pitfalls. This comprises how to deal with overfitting or missing data, how to measure the skill of our algorithm and learn possibilities how to improve the algorithm. Moreover, dealing with imbalanced dataset is our prior problem to get a solution.


# Revision of The Hackathon

We joined the Hackathon with high expectations. We were sure that we will get answers for our questions and concerns regarding to the difficulties that we have come up with. Luckily, our mentor Steffen Seitz, guided us to get great accuracy for our binary classification problem. We set up an architecture of the network and figure out how we can improve the algorithm. Since, we are dealing with imbalanced dataset, his advice about using ROC curve to improve the skills of neural network was a brilliant idea. Thus, we were rewarded one of two best accurate team awards in the Hackathon.

All in all, it was a great experience for us to be a part of the Hackathon. We had a lot of discussion with friendly faces. The organizer of the Hackathon, Peter Steinbach, is always ready for your questions and/or problems regarding to source problems. We turned back to our institute with lots of answers, and good results. We also agreed to start to write down a paper with our mentor so I suggest you to stay in contact with your mentor for the future projects.

A few drops of advice for the future teams:

- Prepare your dataset before going to the Hackathon,
- Have a clear big picture to help your mentor to guide you till to the end of tunnel,
- Since a lot of teams and mentors are around and ready for the discussion anytime, be aware that you can get so much help/ideas but confusion at the same time (remember everyone has a way of doing things)


# Results

We started to check the architecture of the neural network. Afterwards, when we are convinced that the neural network has optimum parameters, we start to increase the size of dataset alternately. We started with 50 thousand samples, then increased the amount till to 5 million. At the end of the week, the size of our dataset was 11 million samples. In figure 6, you see the scatter plot of temperature vs depth of training dataset.

![]({{site.baseurl}}/assets/2019-12-17-salacia-figure6.png)  
__Figure 6__: The training dataset which is %75 of whole dataset (quality flags QF1: Good, quality flags QF2: Bad)

Our algorithm had a good success with predictions of flags of test dataset. Usage of [ROC curve](https://en.wikipedia.org/wiki/Receiver_operating_characteristic) (Figure 7) for our specific problem brings to evaluate probability threshold to get good accuracy for the imbalanced dataset classification problem.

![]({{site.baseurl}}/assets/2019-12-17-salacia-figure7.png)  
__Figure 7__: [ROC](https://en.wikipedia.org/wiki/Receiver_operating_characteristic) curve with [AUC](https://en.wikipedia.org/wiki/Receiver_operating_characteristic#Area_under_the_curve) 0.993

We will now plan to use evaluated probability threshold on different unknown datasets for the trained neural network, and check the accuracy. For the future, we would like to implement the world ocean dataset into our training dataset. For this reason, we will use [ResNet](https://en.wikipedia.org/wiki/Residual_neural_network) to handle with 10 of millions of samples.

Finally, we would like to thank for this great event to be happened and let us to be a part of it. It was a remarkable experience. We were quite satisfied at the end of the event. Many thanks to organizer, mentor, and all participants. 

