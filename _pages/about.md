---
title: About
permalink: /about/
author_profile: false
---

We are a grass-roots movement of Machine Learning practitioners in Dresden, Germany.

## Members of the MLC Organizing Committee

* Dánnell Quesada Chacón
  ([TU Dresden](https://tu-dresden.de/))
* Heide Meißner
  ([HZDR](https://www.hzdr.de))
* Jeffrey Kelling
  ([HZDR](https://www.hzdr.de))
* Vincent Latzko
  ([TU Dresden](https://tu-dresden.de/))
* Peter Steinbach
  ([HZDR](https://www.hzdr.de))
* Peter Winkler
  ([ScaDS](https://www.scads.de/), [TU Dresden](https://tu-dresden.de/))
* Steffen Seitz
  ([TU Dresden](https://tu-dresden.de/))

### Former Members of the MLC Organizing Committee

* Patrick Stiller
  ([HZDR](https://www.hzdr.de))
* Matthias Werner 
  ([HZDR](https://www.hzdr.de))
* Falk Zakrzewski
  ([UKD](https://www.uniklinikum-dresden.de/en))

[![HZDR](/pages/assets/images/partners/hzdr.png)](https://www.hzdr.de)  [![TU Dresden](/pages/assets/images/partners/tud.png)](https://tu-dresden.de/)  [![ScaDS](/pages/assets/images/partners/scads.png)](https://www.scads.de/)  [![MPI-CBG](/pages/assets/images/partners/mpicbg.png)](https://www.mpi-cbg.de)
