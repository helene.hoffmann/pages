#!/bin/bash
#set -e # halt script on error

if [ "$#" -lt 1 ]; then
	echo "Usage: prep_image.sh <srcimg> <dstpath|optional>"
    echo "       will shrink image from <srcimg> to 1600pixels along the largest dimension "
    exit 1
fi

IMG="$1"
NAME="$2"
if [ -z "$NAME" ]; then
	NAME=$( echo ${IMG} | sed 's/\(.*\)\..*/\1/')
fi
SUFFIX=$( echo ${IMG} | sed 's/.*\.//')

DST="$NAME"_1600px."$SUFFIX"
if [ -n $2 ];then
    DST=$2
fi

if [ $(identify -format "%[fx:w<h?1:0]" ${IMG}) -eq 1 ];
then
    echo "running: " convert -resize x1600 ${IMG} ${DST}
    convert -resize 1600 ${IMG} ${DST}
else
    echo "running: " convert -resize 1600x ${IMG} ${DST}
    convert -resize x1600 ${IMG} ${DST}
fi

exit 0

# checkGen thumb 535x357

# if [ ! -f "$NAME"_thumb@2x."$SUFFIX" ]; then
# 	convert -resize 50% "$NAME"_thumb."$SUFFIX" "$NAME"_thumb@2x."$SUFFIX"
# fi

# checkGen lg        1999x1333
# checkGen md        991x661
# checkGen sm        767x511
# checkGen xs        575x383
# checkGen placehold 230x153
# if [ ! -z "$2" ]; then
# 	checkGen "" 1920x1080
# fi
